# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.2] - 2019-08-30

### Bugs fixed

- JSDom was listed as a devDependency instead of a regular dependency, and thus did not get installed.
- The README instructions listed webmention.rocks, typically a testing target to send Webmentions to, as a source instead of a target, which was unclear.

## [1.0.1] - 2019-07-16

### Changes

- No functional changes in this release, but the API documentation is now generated automatically.
  This happens to also fix one instance in which the documentation switched around parameters.

## [1.0.0] - 2019-06-14

### New features

- First release! Provide a source and target URL, then send a Webmention. Includes integration tests using https://Webmention.rocks.
