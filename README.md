webmen
======
A small module to send [Webmentions](https://webmention.net/). Should work in both Node and the browser, although you might want to include polyfills for `Array.from`, `Promise` and `fetch` for the latter.

This project also includes TypeScript type definitions.

## Installation

```bash
npm install webmen --save
```

## Usage

```javascript
import { webmention } from 'webmen';

webmention('http://poop.bike', 'https://webmention.rocks/test/1')
.then((success) => console.log('Did the target at webmention.rocks accept the Webmention?', success))
.catch((error) => console.error('Something went wrong sending a Webmention:', error));
```

## Changelog

See [`CHANGELOG.md`](https://gitlab.com/Flockademic/get-dois/blob/master/CHANGELOG.md).

## License

MIT © [Vincent Tunru](https://vincenttunru.com)
