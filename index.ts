import { discover } from './src/discover';
import { send } from './src/send';

export * from './src/discover'
export * from './src/send'

/**
 * Notify `target` that `source` links to it. You will usually use this method.
 *
 * This should be called when the link to `target` is added, and again every time `source` is updated, or if/when it eventually is deleted.
 * @param source The web page that mentions `target`; it should actually include a link to `target` to be a valid Webmention, or have included one at some point of time in the past.
 * @param target The web page that is mentioned by `source`. webmen will attempt to find a reference to the Webmention endpoint there, to which the actual Webmention will be sent.
 * @returns A Promise that is either rejected, e.g. when the network is down, or resolved with either `true` (`target` accepted the Webmention) or `false` (e.g. `target` does not support Webmentions).
 */
export async function webmention (source: string, target: string): Promise<boolean> {
  const endpoint = await discover(target);

  if (!endpoint) {
    return false;
  }

  const response = await send(endpoint, source, target);
  return response.ok;
}
