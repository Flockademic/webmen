import { discover } from './discover';

async function testTarget(testNr: number, expected?: string) {
  const target = `https://webmention.rocks/test/${testNr.toString()}`;
  const endpoint = await discover(target);
  expect(endpoint).toBe(expected || `https://webmention.rocks/test/${testNr.toString()}/webmention`);
}

for(let testNr = 1; testNr <= 22; testNr++) {
  if (testNr === 15) {
    // This test page is its own endpoint:
    it(
      `should pass Webmention.rocks Discovery test nr. #${testNr}`,
      () => testTarget(testNr, 'https://webmention.rocks/test/15'),
    );
  } else if (testNr === 21) {
    // This test page adds a query string to its endpoint:
    it(
      `should pass Webmention.rocks Discovery test nr. #${testNr}`,
      () => testTarget(testNr, 'https://webmention.rocks/test/21/webmention?query=yes'),
    );
  } else {
    it(`should pass Webmention.rocks Discovery test nr. #${testNr}`, () => testTarget(testNr));
  }
}

it('should pass Webmention.rocks Discovery test nr. #23', async () => {
  // This is a page that redirects, and is hence a different target
  const target = 'https://webmention.rocks/test/23/page';
  const endpoint = await discover(target);
  expect(endpoint).toMatch('https://webmention.rocks/test/23/webmention-endpoint/');
});

it('should return null if no endpoint was found', async () => {
  const target = 'https://example.com';
  const endpoint = await discover(target);
  expect(endpoint).toBeNull();
});
