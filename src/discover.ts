import { resolve } from 'path';
import { URL } from 'url';
import { fetch } from 'cross-fetch';
import { JSDOM } from 'jsdom';
import { parse } from 'http-link-header';

/**
 * Given a `target` that is linked to, finds that page's associated Webmention endpoint, if any.
 *
 * @param target The web page that is mentioned. If the page supports Webmentions, it will include a reference to an endpoint (e.g. `<link rel="webmention" href="https://endpoint.example">`) that a Webmention should be sent to.
 * @returns A Promise that is either rejected, e.g. when the network is down, or resolved. If it is resolved, it will either return the endpoint referred to in the target page, or `null` if it did not refer to an endpoint.
 */
export async function discover(target: string): Promise<string | null> {
  const response = await fetch(target);
  const headers = response.headers
  const headerEndpoints = getHeaderEndpoints(headers);
  if(headerEndpoints.length > 0) {
    return getAbsoluteEndpoint(target, headerEndpoints[0]);
  }
  const html = await response.text();
  const endpoints = getEmbeddedEndpoints(html).map((endpoint) => getAbsoluteEndpoint(target, endpoint));
  return (endpoints.length === 0) ? null : endpoints[0];
}

/**
  * @hidden
  * @param headers HTTP headers returned by an HTTP endpoint.
  * @returns Webmention endpoints referred to in those headers, if any.
 */
export function getHeaderEndpoints(headers: Headers): string[] {
  const linkHeader = headers.get('link');
  if (linkHeader) {
    const header = parse(linkHeader)
    const refs = header.refs.filter((ref) =>  hasWebmention(ref.rel));
    return refs.map((ref) => ref.uri);
  }
  return [];
}

/**
 * @hidden
 * @param html The HTML contents of a webpage.
 * @returns Endpoints referred to (e.g. through `<link rel="webmention" href="https://endpoint.example">`) in the given HTML, if any.
 */
export function getEmbeddedEndpoints(html: string): string[] {
  const dom = new JSDOM(html);
  const elements = Array.from(dom.window.document.querySelectorAll('link[rel],a[rel]'));
  const withRel = elements.filter((element) => {
    const rel = element.getAttribute('rel');
    return (rel)
    ? hasWebmention(rel)
      /* istanbul ignore next [I don't know of a page we can test with a rel attribute that's not a Webmention:] */
      : false;
  });
  const endpoints = withRel.map((element) => element.getAttribute('href')).filter(isDefined);
  return endpoints;
}

/**
 * @hidden
 */
function isDefined<T>(value?: T | null): value is T {
  return (typeof value !== 'undefined') && (value !== null);
}

/**
 * If the `rel` attribute includes `webmention` (e.g. rel="webmention somethingelse"),
 * include this element.
 * 
 * @hidden
 * @param rel The value of the rel attribute in a tag or HTTP Link header
 */
function hasWebmention(rel: string) {
  return rel.split(' ').indexOf('webmention') !== -1;
}

/**
 * @hidden
 * @param target The URL at which the `endpoint` reference was found.
 * @param endpoint The endpoint as it was listed at that URL - which could be a relative URL.
 * @returns An absolute URL.
 */
export function getAbsoluteEndpoint(target: string, endpoint: string) {
  // Although it's not mentioned in the spec
  // (https://www.w3.org/TR/webmention/#sender-discovers-receiver-webmention-endpoint-p-7),
  // apparently an empty string means the target is its own endpoint:
  // https://webmention.rocks/test/15
  if (endpoint === '') { return target; }

  try {
    new URL(endpoint);
    return endpoint;
  } catch(e) {
    // If `endpoint` is not a valid URL, try to resolve it:
    const targetUrl = new URL(target);

    if (endpoint.charAt(0) === '/') {
      // If the endpoint starts with a slash, it's relative to the target page's root:
      const endpointUrl = new URL(endpoint, targetUrl.origin);
      return endpointUrl.href;
    }

    // If the endpoint does not start with a slash, it's relative to the target page:
    const endpointUrl = new URL(resolve(targetUrl.pathname, '../', endpoint), targetUrl.origin);
    return endpointUrl.href;
  }
}
