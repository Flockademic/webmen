import fetch from 'cross-fetch';
import { URLSearchParams } from 'url';

/**
 * Given an endpoint (see [[discover]]), send it a Webmention.
 *
 * @param endpoint An endpoint that is set up to receive Webmentions for a given `target`.
 * @param source The page that links to `target`.
 * @param target A page whose Webmention endpoint is `endpoint`, and which is linked to from `source`.
 */
/* istanbul ignore next [A valid Webmention depends on the source linking to target properly - we don't have a source.] */
export async function send(endpoint: string, source: string, target: string): Promise<Response> {
  const params = new URLSearchParams({ source, target });
  const response = await fetch(endpoint, {
    method: 'POST',
    body: params,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });

  return response;
}
